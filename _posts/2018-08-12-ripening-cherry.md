![Picture of ripening cherry]({{ site.url }}/assets/img/ripening-cherry.jpg "Ripening cherry")

Patiently waiting for the cherry to ripen to the perfect shade to hand pick and begin the process to your ![Picture of coffeecup]({{ site.url }}/assets/icons/coffeecup32x32.png "cup")! 