The [Berlin Coffee Festival](https://berlincoffeefestival.de/programm/) is starting next Friday, August 31st in Berlin, Germany.

An interesting talk on Friday 4pm is about life on a coffee farm: 

> Everything you want to know about a life on a coffee farm.
> ---
> 
> Before they started roasting in 2010, our friends from Doubleshot decided to fully experience the life on a coffee farm. For over a year they have been living and working on different coffee farms in Central America.
> 
> They got to experience how much hard work goes into every cup of coffee we get to drink. There they truly understood what are the core values of Specialty Coffee and created a company which is built on relationships, honesty and quality.
> 
> Listen to their stories about everyday life on a farm, the coffee journey from bean to cup and about the beginnings of one of the best known European roasters.


