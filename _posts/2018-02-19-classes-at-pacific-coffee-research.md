We recently attended two Specialty Coffee Association classes at Pacific Coffee Research, here in Kona. We passed the 'Intro to Coffee' and the 'Sensory Skills Foundation' classes and cupped our coffee. There is so much to learn and we will continue our classes.
As a former teacher, I abide by the "Never Stop Learning" philosophy and thoroughly enjoyed the instruction by Brittany and Brian. Highly recommend!

![Cupping coffee at the Intro to Coffee and Sensory Skills Foundation Class]({{ site.url }}/assets/img/cupping-coffee-at-intro-to-coffee-and-sensory-skills-foundation-class.jpg)
