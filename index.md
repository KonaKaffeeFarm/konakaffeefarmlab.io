---
layout: welcome
header:
  logo: /assets/img/kkflogo.png
  overlay_image: /assets/img/green-coffee-beans_960px.jpg
  overlay_filter: "0.5"
  image_description: "Dedicated to quality"
  cta_label: "Buy our coffee now on Etsy!"
  cta_url: https://etsy.com/shop/Konakaffeefarm
excerpt: 100% pure Kona Coffee
---

Aloha! We planted our 400 trees several years ago, and now they are producing high quality beans.  
Organically grown, hand picked, sun dried and processed by us here in the Kona Coffee Belt on the Big Island of Hawaii, roasted to a medium plus.  
Experience the taste that only pure estate and organically grown 100% Kona can provide!

From our farm to your cup - Aloha!

![Get Real! 100% Kona Coffee]({{ site.url }}{{ site.baseurl }}/assets/logos/kcfa_get-real-kona-coffee-logo.png){:width="30%"}
[![Kona Coffee Farmers Association Logo]({{ site.url }}{{ site.baseurl }}/assets/img/kona-coffee-farmers-association-logo.jpg){:width="30%"}](https://www.konacoffeefarmers.org)
[![Specialty Coffee Association Logo]({{ site.url }}{{ site.baseurl }}/assets/img/specialty-coffee-association-logo.jpg){:width="30%"}](https://sca.coffee)
