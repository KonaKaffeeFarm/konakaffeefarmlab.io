---
layout: single
title: Pay Now using PayPal
permalink: /paynow/
---
<script>
<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
<input type="hidden" name="cmd" value="_s-xclick">
<input type="hidden" name="hosted_button_id" value="Y7KGKQL5JTCCU">
<table>
<tr><td><input type="hidden" name="on0" value="Select weight">Select weight</td></tr><tr><td><select name="os0">
	<option value="8 oz (227 gr)">8 oz (227 gr) $15.00 USD</option>
	<option value="16 oz (454 gr)">16 oz (454 gr) $25.00 USD</option>
</select> </td></tr>
</table>
<input type="hidden" name="currency_code" value="USD">
<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_paynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
</form>
</script>