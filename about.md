---
layout: single
title: About
permalink: /about/
---

Aloha from Opihihale, south of Captain Cook, in the South Kona district on the Big Island of Hawaii. We are a former business owner and a teacher from Pittsburgh, PA, now proud coffee farmers in the glorious Kona Coffee Belt. We planted our 400 'Kona Typica' coffee trees at 1200 feet, and are farming organically. Our trees are pruned, fertilized, and nurtured to grow and provide the unique, fruity taste that Kona Coffee is known for worldwide. We hand pick, sun dry, and sell only our beans - thus we are labeled "Estate Coffee".

We are members of the [Kona Coffee Farmers Association](https://www.konacoffeefarmers.org/kcfa-business/membership-seal-program), and we are constantly learning and improving our farming methods and processing practices. Our goal is to grow the best coffee we can! We are excited to reach new customers with green and roasted beans and promise you will enjoy our farm's bounty.

![Picture of sunset over Kona Kaffee Farm]({{ site.url }}{{ site.baseurl }}/assets/img/sunset-over-kona-kaffee-farm_705px.jpg "Sunset over Kona Kaffee Farm")

The website was created with Jekyll, is served from [GitLab Pages](https://gitlab.com/KonaKaffeeFarm/konakaffeefarm.gitlab.io), and proudly runs without advertisement, AdTech-scripts, web tracking, fingerprinting or any other kind of privacy-infringing methods. This site also does not need cookies to be set on your device. The website will function just fine without any of the aforementioned webcruft.

